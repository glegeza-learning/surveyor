import axios from 'axios';
import * as actions from './types';

const currentUserRoute = '/api/current_user';
const stripeRoute = '/api/stripe';

export const fetchUser = () => async (dispatch) => {
    const res = await axios.get(currentUserRoute);
    dispatch({type: actions.FETCH_USER, payload: res.data});
};

export const handleToken = (token) => async (dispatch) => {
    dispatch({type: actions.START_FETCH_UPDATE});
    const res = await axios.post(stripeRoute, token);
    dispatch({type: actions.FETCH_USER, payload: res.data});
}

export const submitSurvey = (values, history) => async (dispatch) => {
    const res = await axios.post('/api/surveys', values);
    history.push('/surveys');
    dispatch({type: actions.FETCH_USER, payload: res.data});
}

export const fetchSurveys = () => async (dispatch) => {
    const res = await axios.get('/api/surveys');

    dispatch({type: actions.FETCH_SURVEYS, payload: res.data});
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}