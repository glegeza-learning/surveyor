import React from 'react';
import './DotLoader.css';

const dotLoader = () => {
    return (
        <div className="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
    )
};

export default dotLoader;