import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import Payments from './Payments';
import DotLoader from './ui/DotLoader';

class Header extends Component {
    renderContent = () => {
        switch(this.props.auth) {
            case null:
                return;
            case false:
                return <li><a href="/auth/google">Login With Google</a></li>;
            default:
                let returnArray = this.renderCredits();
                returnArray.push(<li key={3}><a href="/api/logout">Logout</a></li>);
                return returnArray;
        }
    }

    renderCredits = () => {
        return this.props.auth.credits === undefined
            ? [<li key={1}><DotLoader /></li>]
            : [
                <li key={1}><Payments /></li>,
                <li key={2} style={{margin: '0px 10px'}}>{`Credits: ${this.props.auth.credits}`}</li>
            ];
    }

    render() {
        return (
            <nav>
                <div className="nav-wrapper">
                    <Link 
                        to={this.props.auth ? '/surveys' : '/'}
                        className="left brand-logo"
                        style={{margin: '0px 10px'}}
                    >
                        {process.env.REACT_APP_NAME}
                    </Link>
                    <ul id="nav-mobile" className="right">
                        {this.renderContent()}
                    </ul>
                </div>
            </nav>
        );
    }
};

const mapStateToProps = ({auth}) => {
    return {auth};
};

export default connect(mapStateToProps)(Header);