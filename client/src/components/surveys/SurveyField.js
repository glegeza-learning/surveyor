import React from 'react';

const surveyField = ({input, label, meta: {active, error, touched}}) => {
    return (
        <div className="input-field">
            <label className={active || input.value ? 'active' : ''} htmlFor={input.name}>{label}</label>
            <input id={input.name} {...input} style={{marginBottom: '5px'}}/>
            <div className="red-text" style={{marginBottom: '20px'}}>
                {touched && error}
            </div>
        </div>
    );
}

export default surveyField;