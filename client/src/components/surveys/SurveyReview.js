import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import formFields from './formFields';
import * as actions from '../../actions';

const surveyReview = ({onCancel, formValues, submitSurvey, history}) => {

    const fields = formFields.map((f) => (
        <div key={f.name}>
            <label>{f.label}</label>
            <div>{formValues[f.name]}</div>
        </div>
    ));

    return (
        <div>
            <h5>Please confirm your entries</h5>
            <div>
                {fields}
            </div>
            <button
                className="yellow darken-3 white-text btn-flat left"
                onClick={onCancel}>
                Back
            </button>
            <button
                className="green btn-flat right white-text"
                onClick={() => submitSurvey(formValues, history)}>
                Send Survey
                <i className="material-icons right">email</i>
            </button>
        </div>
    );
}

function mapStateToProps(state) {
    return {
        formValues: state.form.surveyForm.values,
    };
}

const connectFunc = connect(mapStateToProps, actions);

export default connectFunc(withRouter(surveyReview));
