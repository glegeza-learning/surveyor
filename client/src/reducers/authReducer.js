import {FETCH_USER, START_FETCH_UPDATE} from '../actions/types';

const authReducer = (state = null, action) => {
    switch(action.type) {
        case FETCH_USER:
            return action.payload || false;
        case START_FETCH_UPDATE:
            return {...state, credits: undefined};
        default:
            return state;
    }
};

export default authReducer;